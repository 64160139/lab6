package com.fah.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BokkBankTest {
    @Test
    public void shouldWithdrawCuccess(){
        BookBank book = new BookBank("fah" , 100.0);
        book.withdraw(50.0);
        assertEquals(50.0, book.getbalance(),0.0001);
    }

    @Test
    public void shouldWithdrawOverBalancel(){
        BookBank book = new BookBank("fah" , 100.0);
        book.withdraw(150.0);
        assertEquals(100.0, book.getbalance(),0.0001);
    }

    @Test
    public void shouldWithdrawNagativeNumber(){
        BookBank book = new BookBank("fah" , 100.0);
        book.withdraw(-50.0);
        assertEquals(100.0, book.getbalance(),0.0001);
    }

    @Test
    public void shoulddeposit(){
        BookBank book = new BookBank("fah" , 100.0);
        book.deposit(50.0);
        assertEquals(150.0, book.getbalance(),0.0001);
    }
    @Test
    public void shoulddepositNagativeNumber(){
        BookBank book = new BookBank("fah" , 100.0);
        book.deposit(-50.0);
        assertEquals(100.0, book.getbalance(),0.0001);
    }
}
