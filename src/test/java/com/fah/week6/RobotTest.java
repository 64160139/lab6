package com.fah.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess1(){
        Robot robot = new Robot("Robot" , 'P',10 , 11);
        assertEquals("Robot",robot.getName());
        assertEquals('P',robot.getSymbol());
        assertEquals(10,robot.getX());
        assertEquals(11,robot.getY());
    }
    @Test
    public void shouldCreateRobotSuccess2(){
        Robot robot = new Robot("Robot" , 'P');
        assertEquals("Robot",robot.getName());
        assertEquals('P',robot.getSymbol());
        assertEquals(0,robot.getX());
        assertEquals(0,robot.getY());
    }
    @Test
    public void shouldDownOver(){
        Robot  ro = new Robot("Robot" , 'H', 0 , Robot.MAX_Y);
        assertEquals(false, ro.down());
        assertEquals(Robot.MAX_Y, ro.getY());
    }
    @Test
    public void shouldUpNagative(){
        Robot  ro = new Robot("Robot" , 'H', 0 , Robot.MIN_Y);
        assertEquals(false, ro.up());
        assertEquals(Robot.MIN_Y, ro.getY());
    }
    @Test
    public void shouldUpSucces(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 11);
        boolean result =  ro.up();
        assertEquals(10, ro.getY());
    }
    @Test
    public void shouldUpNSucces1(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 11);
        boolean result =  ro.up(5);
        assertEquals(true, result);
        assertEquals(6, ro.getY());
    }
    @Test
    public void shouldUpNSucces2(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 11);
        boolean result =  ro.up(11);
        assertEquals(true, result);
        assertEquals(0 , ro.getY());
    }
    @Test
    public void shouldUpNfail1(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 11);
        boolean result =  ro.up(12);
        assertEquals(false, result);
        assertEquals(0 , ro.getY());
    }

    @Test
    public void shouldDownSucces(){
        Robot  ro = new Robot("Robot" , 'H', 0 , 1);
        assertEquals(true, ro.up());
        assertEquals(0, ro.getY());
    }
    @Test
    public void shouldDownNSucces1(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 10);
        boolean result = ro.down(5);
        assertEquals(true, result);
        assertEquals(15, ro.getY());
    }
    @Test
    public void shouldDownNSucces2(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 10);
        boolean result = ro.down(9);
        assertEquals(true, result);
        assertEquals(19, ro.getY());
    }
    @Test
    public void shouldDownNfail1(){
        Robot  ro = new Robot("Robot" , 'H', 10 , 10);
        boolean result = ro.down(10);
        assertEquals(false, result);
        assertEquals(19, ro.getY());
    }

    @Test
    public void shouldleftSucces(){
        Robot  ro = new Robot("Robot" , 'H', 1 , 0);
        assertEquals(true, ro.left());
        assertEquals(0, ro.getX());
    }
    @Test
    public void shouldleftNSucces1(){
        Robot  ro = new Robot("Robot" , 'H', 12 , 10);
        boolean result = ro.left(7);
        assertEquals(true, result);
        assertEquals(5, ro.getX());
    }
    @Test
    public void shouldleftNSucces2(){
        Robot  ro = new Robot("Robot" , 'H', 12 , 10);
        boolean result = ro.left(12);
        assertEquals(true, result);
        assertEquals(0, ro.getX());
    }
    @Test
    public void shouldleftNfail1(){
        Robot  ro = new Robot("Robot" , 'H', 2 , 10);
        boolean result = ro.left(3);
        assertEquals(false, result);
        assertEquals(0, ro.getX());
    }
    @Test
    public void shouldrightSucces(){
        Robot  ro = new Robot("Robot" , 'H', 0 , 1);
        assertEquals(true, ro.right());
        assertEquals(1, ro.getY());
    }
    @Test
    public void shouldrightNSucces1(){
        Robot  ro = new Robot("Robot" , 'H', 0 , 1);
        boolean result = ro.right(3);
        assertEquals(true, result);
        assertEquals(3, ro.getX());
    }

    @Test
    public void shouldrightNSucces2(){
        Robot  ro = new Robot("Robot" , 'H', 15 , 1);
        boolean result = ro.right(4);
        assertEquals(true, result);
        assertEquals(19, ro.getX());
    }
    
    @Test
    public void shouldrightNfail1(){
        Robot  ro = new Robot("Robot" , 'H', 15 , 1);
        boolean result = ro.right(6);
        assertEquals(false, result);
        assertEquals(19, ro.getX());
    }
}
