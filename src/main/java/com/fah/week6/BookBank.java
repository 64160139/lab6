package com.fah.week6;

import com.sun.jdi.DoubleValue;

public class BookBank {
    private String name;
    private double balance ;

    public BookBank(String name , double balance){
        this.name = name;
        this.balance = balance;
    }

    public boolean deposit(double money ){
        if (money < 1) return false ;
        balance = balance + money;
        return true;
    } 
    public boolean withdraw(double money ){
        if (money > balance) return false;
        if (money < 1 ) return false;
        balance = balance - money;
        return true;
    }
    public void print(){
        System.out.println(name+" "+balance);
    }
    public String getname(){
        return name;
    }
    public double getbalance(){
        return balance;
    } 
}
