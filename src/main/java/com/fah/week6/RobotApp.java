package com.fah.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot kapong = new Robot("garie",'G',1,0);
        Robot blue = new Robot("Robot",'B',10,10);
        kapong.print();
        kapong.right();
        kapong.print();
        blue.print();

        for(int y = Robot.MIN_Y ; y < Robot.MAX_Y ; y ++ ){
            for(int x = Robot.MIN_X ; x < Robot.MAX_X ; x ++ ){
                if( kapong.getX() == x && kapong.getY() == y ){
                    System.out.print(kapong.getSymbol());
                } else if (blue.getX() == x && blue.getY() == y ){
                    System.out.print(blue.getSymbol());
                } else{
                    System.out.print("-");
                }                 
            }
            System.out.println();
        }
    }
}
