package com.fah.week6;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank pongpipat = new BookBank("pongpipat" , 50.0); 
        pongpipat.print();

        BookBank pongpong = new BookBank("pongpong" ,100.0);
        pongpong.print();

        pongpong.withdraw(20.0);
        pongpong.print();

        pongpipat.deposit(40000.0);
        pongpipat.print();

        BookBank thai = new BookBank("Thai" , 1000000.0);
        thai.print();
        thai.deposit(200000.0);
        thai.withdraw(5000.0);
        thai.print();
    } 
}
